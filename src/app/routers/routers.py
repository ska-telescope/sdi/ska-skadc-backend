import logging
import os

import aiohttp
from fastapi import APIRouter, status
from fastapi.responses import JSONResponse
from ska_cicd_services_api import nexus

from app.models.common import get_mongodb
from app.models.helm_dependencies import HelmDependencies
from app.models.python_dependencies import PythonDependencies

router = APIRouter()

logger = logging.getLogger("gunicorn.error")


def list_dependencies_database(dependencies_main: dict, db):

    logger.info(dependencies_main)

    for dependencies in dependencies_main["dependencies"]:

        if (
            "deprecated" not in dependencies
            or dependencies["deprecated"] is False
        ):
            sub_dependencies = db.find_documents(
                {
                    "format": dependencies["format"],
                    "name": dependencies["name"],
                    "version": dependencies["version"],
                }
            )

            if sub_dependencies:
                sub_dependencies = list_dependencies_database(
                    sub_dependencies[0], db
                )
                dependencies["dependencies"] = sub_dependencies["dependencies"]
            else:
                dependencies["dependencies"] = []
        else:
            dependencies["dependencies"] = []

    return dependencies_main


@router.post("/helm-all/", response_description="All helm dependencies")
async def create_task_helm():
    url = os.getenv("NEXUS_URL")

    db = get_mongodb()

    async with aiohttp.ClientSession() as session:
        api = nexus.NexusApi(session, url=url)
        helm = HelmDependencies(api)
        list_all_artefacts = await helm.get_list_helm_car()
        latest_version_list_helm = await helm.get_latest_version_list(
            list_all_artefacts
        )
        number_of_artefacts = len(list_all_artefacts)
        artefacts_collected = 0
        for artefacts in list_all_artefacts:
            if not db.find_documents(
                {
                    "format": "helm",
                    "name": artefacts["name"],
                    "version": artefacts["version"],
                }
            ):
                task = await helm.get_artefact_info(
                    artefacts["name"],
                    artefacts["version"],
                    latest_version_list_helm,
                )

                db.add_artifact_metadata([task])
            artefacts_collected = artefacts_collected + 1
            print(
                "Percentage Done: ",
                int(float(artefacts_collected / number_of_artefacts) * 100),
            )

    all_documents = db.find_documents({"format": "helm"})

    return JSONResponse(
        status_code=status.HTTP_201_CREATED, content=all_documents
    )


@router.post("/python-all/", response_description="All python dependencies")
async def create_task_python():
    url = os.getenv("NEXUS_URL")

    db = get_mongodb()

    async with aiohttp.ClientSession() as session:
        api = nexus.NexusApi(session, url=url)
        python = PythonDependencies(api)
        list_all_artefacts = await python.get_list_python_car()
        latest_version_list_helm = await python.get_latest_version_list(
            list_all_artefacts
        )
        number_of_artefacts = len(list_all_artefacts)
        artefacts_collected = 0
        for artefacts in list_all_artefacts:
            if not db.find_documents(
                {
                    "format": "python",
                    "name": artefacts["name"],
                    "version": artefacts["version"],
                }
            ):
                task = await python.get_artefact_info(
                    artefacts["name"],
                    artefacts["version"],
                    latest_version_list_helm,
                )

                db.add_artifact_metadata([task])
            artefacts_collected = artefacts_collected + 1
            print(
                "Percentage Done: ",
                int(float(artefacts_collected / number_of_artefacts) * 100),
            )

    all_documents = db.find_documents({"format": "helm"})

    return JSONResponse(
        status_code=status.HTTP_201_CREATED, content=all_documents
    )


@router.get("/", response_description="List all tasks")
async def list_tasks():
    db = get_mongodb()
    all_documents = db.list_all_documents()
    return JSONResponse(
        status_code=status.HTTP_201_CREATED, content=all_documents
    )


@router.get("/get/", response_description="Get a single document")
async def show_task(artefact_name: str):
    db = get_mongodb()
    results = db.find_documents_index(artefact_name)

    return JSONResponse(status_code=status.HTTP_201_CREATED, content=results)


@router.get(
    "/get_name_dependencies/", response_description="Get document dependencies"
)
async def show_task_dependencies(
    format: str, artefact_name: str, artefact_version: str
):
    results = None
    db = get_mongodb()
    results = db.find_documents(
        {"format": format, "name": artefact_name, "version": artefact_version}
    )
    if results:
        results = list_dependencies_database(results[0], db)

    return JSONResponse(status_code=status.HTTP_201_CREATED, content=results)
