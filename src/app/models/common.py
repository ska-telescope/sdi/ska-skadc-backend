import os

from pymongo.mongo_client import MongoClient

# from mongodb_api import MongoDBApi
from app.models.mongodb_api import MongoDBApi


async def get_list_from_async_gen(async_generator):
    return [i async for i in async_generator]


async def search_components(nexus_repository, artefact_name, api):
    result = {}
    version_list = []
    counter = 0

    artefacts = await api.get_components_list(nexus_repository)
    async for art in artefacts:
        if art["name"] == artefact_name:
            d1 = {
                str(counter): {
                    "id": art["id"],
                    "format": art["format"],
                    "version": art["version"],
                }
            }
            result.update(d1)
            version_list.append(art["version"])
            counter += 1

    return result, version_list


async def search_all_components(nexus_repository, api):

    artefacts = await api.get_components_list(nexus_repository)
    artefacts = await get_list_from_async_gen(artefacts)
    return artefacts


def get_mongodb():

    client = MongoClient(os.getenv("MONGO_URL"))
    db = MongoDBApi(client)
    db.select_database(os.getenv("MONGO_DB"))
    db.select_collection(os.getenv("MONGO_COL"))
    return db


# def get_latest_version(artefact_list):
