import io
import os
import re
import shutil
import tarfile
from distutils.core import run_setup

import git
import toml
from packaging import version
from ska_cicd_artefact_validations.validation.controllers.component_manager import (  # NOQA: E501
    PythonHandler,
)
from ska_cicd_artefact_validations.validation.models.component import Component

# from common import search_all_components, search_components
from app.models.common import search_all_components, search_components

# import aiohttp
# from ska_cicd_services_api import nexus
# import asyncio


class PythonDependencies(PythonHandler):
    def get_python_dependencies(
        self, asset, latest_version_list_python, metadata
    ):

        python_dependencies = {}
        list_of_dependencies = []

        with tarfile.open(asset, "r") as archive:
            python_dependencies_files = [
                file
                for file in archive.getmembers()
                if "pyproject.toml" in file.name
                or "setup.py" in file.name
                or "requirements.txt" in file.name
            ]

            if "setup.py" in str(python_dependencies_files):
                python_dependencies = self.setup_dependencies(
                    archive, python_dependencies, metadata
                )

            if "pyproject.toml" in str(python_dependencies_files):
                python_dependencies = self.poetry_dependencies(
                    archive, python_dependencies
                )

            if "requirements.txt" in str(python_dependencies_files):
                python_dependencies = self.requirements_dependencies(
                    archive, python_dependencies
                )

        for dependencies_name in python_dependencies:

            # for examples like ska_ser_logging
            if "ska_" in dependencies_name:
                fixed_dependencies_name = str(dependencies_name).replace(
                    "_", "-"
                )
            else:
                fixed_dependencies_name = dependencies_name

            if fixed_dependencies_name in latest_version_list_python:
                latest_version = latest_version_list_python[
                    fixed_dependencies_name
                ]
            else:
                if dependencies_name == "python":
                    latest_version = "3.10.0"
                else:
                    latest_version = "latest"

            # Remove some bugs cause by setup.py parsing
            if (
                " " not in dependencies_name
                and "--extra-index" not in dependencies_name
            ):
                dependencies_version = python_dependencies[dependencies_name]
                # TODO remove hardocode for this example
                if "ska_cicd_services_api-" in dependencies_version:
                    dependencies_version = str(dependencies_version).replace(
                        "ska_cicd_services_api-", ""
                    )

                if "ska_cicd_artefact_validations-" in dependencies_version:
                    dependencies_version = str(dependencies_version).replace(
                        "ska_cicd_artefact_validations-", ""
                    )

                if "*" in dependencies_version:
                    dependencies_version = str(dependencies_version).replace(
                        "*", "latest"
                    )

                list_of_dependencies.append(
                    {
                        "format": "python",
                        "name": fixed_dependencies_name,
                        "version": dependencies_version,
                        "latest_version": latest_version,
                    }
                )

        return list_of_dependencies

    def setup_dependencies(self, archive, python_dependencies, metadata):

        current_dir = ""

        archive.extractall()
        file_path = str(archive.name).replace(".tar.gz", "")
        try:
            result = run_setup(file_path + "/setup.py", stop_after="init")
            python_dependencies = self.parse_setup_dependencies(
                result, python_dependencies
            )

        except FileNotFoundError as e:

            try:
                print("FILES MISSING Clone NEEDED", e)
                repo = git.Repo.clone_from(
                    metadata["CI_PROJECT_URL"], "repo_clone"
                )
                repo.git.checkout(metadata["CI_COMMIT_SHA"])

                current_dir = str(os.getcwd())

                os.chdir(current_dir + "/repo_clone/")

                result = run_setup("setup.py", stop_after="init")
                os.chdir(current_dir)

                python_dependencies = self.parse_setup_dependencies(
                    result, python_dependencies
                )

            except Exception as e:

                print(
                    "FAILED TO BUILD PACKAGE AFTER CLONE. Exception: ",  # NOQA: E501
                    e,
                )

        except Exception as e:
            print("FAILED TO BUILD PACKAGE. Exception: ", e)

        if os.path.isdir(current_dir + "/repo_clone/"):
            shutil.rmtree(current_dir + "/repo_clone/")
        if os.path.isdir(file_path):
            shutil.rmtree(file_path)
        if os.path.isdir(".eggs"):
            shutil.rmtree(".eggs")
        if os.path.isdir("MANIFEST.ska.int"):
            shutil.rmtree("MANIFEST.ska.int")

        return python_dependencies

    def parse_setup_dependencies(self, result, python_dependencies):

        setup_dependencies = result.install_requires
        setup_dependencies.extend(result.setup_requires)

        for dependencies in setup_dependencies:
            if "<" in dependencies:
                dependencie_name = str(dependencies).split("<")[0]
                dependencie_version = "<" + str(dependencies).split("<")[1]
                python_dependencies.update(
                    {dependencie_name: dependencie_version}
                )
            elif ">" in dependencies:
                dependencie_name = str(dependencies).split(">")[0]
                dependencie_version = ">" + str(dependencies).split(">")[1]
                python_dependencies.update(
                    {dependencie_name: dependencie_version}
                )
            elif "==" in dependencies:
                dependencie_name = str(dependencies).split("==")[0]
                dependencie_version = str(dependencies).split("==")[1]
                python_dependencies.update(
                    {dependencie_name: dependencie_version}
                )
            else:
                python_dependencies.update({str(dependencies): "latest"})

        return python_dependencies

    def poetry_dependencies(self, archive, python_dependencies):

        pyproject_file = [
            file
            for file in archive.getmembers()
            if "pyproject.toml" in file.name
        ]
        with io.TextIOWrapper(
            archive.extractfile(pyproject_file[0]),
            encoding="utf-8",
        ) as fd:
            raw_config = fd.read()
            parsed_toml = toml.loads(raw_config)
            if "tool" in parsed_toml:
                if "poetry" in parsed_toml["tool"]:
                    if "dependencies" in parsed_toml["tool"]["poetry"]:
                        python_dependencies.update(
                            parsed_toml["tool"]["poetry"]["dependencies"]
                        )

        return python_dependencies

    def requirements_dependencies(self, archive, python_dependencies):
        requirements_file = [
            file
            for file in archive.getmembers()
            if "requirements.txt" in file.name
        ]
        with io.TextIOWrapper(
            archive.extractfile(requirements_file[0]),
            encoding="utf-8",
        ) as fd:
            for line in fd:
                line = (str(line).replace("\n", "")).replace(" ", "")
                if "<" in line:
                    dependencie_name = (line).split("<")[0]
                    dependencie_version = "<" + str(line).split("<")[1]
                    python_dependencies.update(
                        {dependencie_name: dependencie_version}
                    )
                elif ">" in line:
                    dependencie_name = str(line).split(">")[0]
                    dependencie_version = ">" + str(line).split(">")[1]
                    python_dependencies.update(
                        {dependencie_name: dependencie_version}
                    )
                elif "==" in line:
                    dependencie_name = str(line).split("==")[0]
                    dependencie_version = str(line).split("==")[1]
                    python_dependencies.update(
                        {dependencie_name: dependencie_version}
                    )
                else:
                    python_dependencies.update({str(line): "latest"})

        return python_dependencies

    async def get_artefact_info(
        self, artefact_name, artefact_version, latest_version_list_python
    ):

        dependencies = []

        print("N: ", artefact_name, "V: ", artefact_version)
        result, _ = await search_components(
            "pypi-internal", artefact_name, self.api
        )

        for index in result:
            if result[index]["version"] == artefact_version:
                component = Component(
                    result[index]["id"],
                    artefact_name,
                    artefact_version,
                    "python",
                )
                assets = await self.download(component)
                component._assets = assets
                metada = await self.get_metadata(component)
                for asset in assets:
                    if re.search(r".tar.gz", asset):
                        dependencies = self.get_python_dependencies(
                            asset, latest_version_list_python, metada
                        )

                await self.delete_downloaded_components(component)

        return {
            "format": "python",
            "name": artefact_name,
            "version": artefact_version,
            "dependencies": dependencies,
        }

    async def get_list_python_car(self):

        result = await search_all_components("pypi-internal", self.api)
        return result

    async def get_latest_version_list(self, list_all_artefacts):
        list_latest_version = {}

        for artefact in list_all_artefacts:
            if artefact["name"] not in list_latest_version or version.parse(
                list_latest_version[artefact["name"]]
            ) < version.parse(artefact["version"]):
                list_latest_version.update(
                    {artefact["name"]: artefact["version"]}
                )

        return list_latest_version


# async def main():
#     url = os.getenv("NEXUS_URL")
#     artefact_name_list = ["ska-ser-logging", "ska-tango-examples", "ska-tmc-cdm"] # NOQA: E501
#     artefact_version_list = ["0.4.0", "0.4.16", "6.0.4", "0.4.1", "0.2.23"] # NOQA: E501

#     async with aiohttp.ClientSession() as session:
#             api = nexus.NexusApi(session, url=url)
#             python = PythonDependencies(api)

#             list_all_artefacts = await  python.get_list_python_car()

#             list_latest_version = await python.get_latest_version_list(list_all_artefacts) # NOQA: E501

#             for index in range(0, len(artefact_name_list)):

#                 dependencies = await python.get_artefact_info(artefact_name_list[index], artefact_version_list[index], list_latest_version) # NOQA: E501

#                 print(dependencies)

# if __name__ == "__main__":
#     asyncio.run(main())
