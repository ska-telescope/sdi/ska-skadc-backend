from pydantic import BaseSettings


class CommonSettings(BaseSettings):
    APP_NAME: str = "SKA-Dependencies"
    DEBUG_MODE: bool = False


class ServerSettings(BaseSettings):
    HOST: str = "0.0.0.0"
    PORT: int = 8000


class DatabaseSettings(BaseSettings):
    MONGO_URL: str
    MONGO_DB: str


class Settings(CommonSettings, ServerSettings, DatabaseSettings):
    pass


settings = Settings()
