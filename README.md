# ska-cicd-dependecies-check


## Local Development

### General Workflow

Install [Poetry](https://python-poetry.org/) for Python package and environment management. This project is structured to use this tool for dependency management and publishing, either install it from the website or using below command:

```console
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```

However if you want to use other tools (`Pipenv` or any other tool that can work with `requirements.file`) run the following script to generate a requirements file.

```console
pip install Poetry
make exportlock
```

This will generate `requirements.txt` and `requirements-dev.txt` files for runtime and development environment.

#### _What is Poetry and Why are we using it_

**Poetry** is a python dependency management and packaging tool. It is similar to [pipenv](https://pypi.org/project/pipenv/) for dependency management but it is more actively developed. It organizes dependencies in separate sections of the same file using `pyproject.toml`, described in [PEP 518](https://www.python.org/dev/peps/pep-0518/) so it can specify publishing information and configure installed tools (like black, isort, tox etc.) which makes it easy to both configure and manage dependencies and publishing.


Next, you need to define the variables that are used in each plugins (see plugins respected README files for the all variables) in your `PrivateRules.mak` (needed for makefile targets) and, `.env` file (needed tp tests from vscode and interactive docker development). i.e. for GitLab MR and JIRA Support services:

```console
GITLAB_API_PRIVATE_TOKEN=
GITLAB_API_REQUESTER=
NEXUS_URL=
NEXUS_API_PASSWORD=
NEXUS_API_USERNAME=
NEXUS_DOCKER_QUARANTINE_REPO=q
MAIN_DOCKER_REGISTRY_HOST=
MONGO_URL=mongodb://localhost:27017/
MONGO_DB=dependencies
MONGO_COL=dependencies
```

Now, the project is ready for local development.


Run the command :

```
make oci-image-build
make docker_development
python3 src/main.py
```


```
docker run -d  --name mongo-on-docker  -p 27017:27017 mongo:3.6-stretch
```

```
http://localhost:8000/docs
```