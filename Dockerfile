FROM python:3.10

WORKDIR /home/backend

# Install Poetry
RUN pip install poetry

# Copy poetry.lock* in case it doesn't exist in the repo
COPY pyproject.toml poetry.lock* ./


RUN poetry export -f requirements.txt --output requirements.txt --without-hashes

RUN pip install --no-cache-dir --upgrade -r requirements.txt

COPY src src

CMD python3 src/main.py