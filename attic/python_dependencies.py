
import aiohttp
from ska_cicd_services_api import nexus
import io
import logging
import os
import re
import tarfile
from zipfile import ZipFile
import asyncio
from distutils.core import run_setup
import shutil
import toml


async def search_components(nexus_repository, component_name):
    result = {}
    async with aiohttp.ClientSession() as session:
        url = os.getenv("NEXUS_URL")
        logging.debug("NEXUS URL: %s", url)
        api = nexus.NexusApi(session, url=url)
        artefacts = await api.get_components_list(nexus_repository)
        async for art in artefacts:
            if art["name"] == component_name:
                result = {
                    "id": art["id"],
                    "format": art["format"],
                }
                return result
    return result


class PythonHandler:
    def __init__(self) -> None:
        self._chunk_size = 10 * 1024
    #     self.api = api
    #     self._downloader = ArchiveDownloader(self.api)

    async def search_components(self,nexus_repository, artefact_name):
        result = {}
        counter = 0
        async with aiohttp.ClientSession() as session:
            url = os.getenv("NEXUS_URL")
            logging.debug("NEXUS URL: %s", url)
            api = nexus.NexusApi(session, url=url)
            artefacts = await api.get_components_list(nexus_repository)
            async for art in artefacts:
                if art["name"] == artefact_name:
                    d1 = { str(counter): {
                        "id": art["id"],
                        "format": art["format"],
                        "version": art["version"]
                    }}
                    result.update(d1)
                    counter += 1
        return result

    async def download(self, component_id: str):

        async with aiohttp.ClientSession() as session:
            url = os.getenv("NEXUS_URL")
            logging.debug("NEXUS URL: %s", url)
            api = nexus.NexusApi(session, url=url)
            component = await api.get_component_info(component_id)
            assets = []
            for asset in component["assets"]:
                download_url = asset["downloadUrl"]
                asset_name = download_url.split("/")[-1]
                asset_name = await self._unique_name(asset_name)
                async with aiohttp.ClientSession() as session:
                    async with session.get(download_url) as resp:
                        with open(f"{asset_name}", "wb") as fd:
                            while True:
                                chunk = await resp.content.read(self._chunk_size)
                                if not chunk:
                                    break
                                fd.write(chunk)
                            # Note: this assumes path is not changed,
                            # if so use os.path.realpath(fd.name)
                            assets.append(fd.name)
            return assets
    
    async def _unique_name(self, filename):
        num = 1
        while os.path.isfile(filename):
            num = num + 1
            filename = "{}_{}".format(num, filename)
        return filename

    def get_python_dependencies(self,asset):
        setup_dependencies = []
        requirements_dependencies = []
        python_dependencies = {}
        lines = ""
        with tarfile.open(asset, "r") as archive:
            python_dependencies_files = [
                file
                for file in archive.getmembers()
                if "pyproject.toml" in file.name or "setup.py" in file.name or "requirements.txt" in file.name
            ]

            for python_dep_file in python_dependencies_files:
                
                if "setup.py" in str(python_dep_file):
                    archive.extractall()
                    file_path=str(archive.name).replace(".tar.gz","") 
                    result = run_setup(file_path + "/setup.py", stop_after="init")
                    if os.path.isdir(file_path):
                        shutil.rmtree(file_path)
                    if os.path.isdir(".eggs"):
                        shutil.rmtree(".eggs")

                    setup_dependencies = result.install_requires
                    setup_dependencies.extend(result.setup_requires)

                    for dependencies in setup_dependencies:
                        if "<" in dependencies:
                            dependencie_name=str(dependencies).split("<")[0]
                            dependencie_version= "<" + str(dependencies).split("<")[1]
                            python_dependencies.update({dependencie_name : dependencie_version})
                        elif ">" in dependencies:
                            dependencie_name=str(dependencies).split(">")[0]
                            dependencie_version= ">" + str(dependencies).split(">")[1]
                            python_dependencies.update({dependencie_name : dependencie_version})                           
                        elif "==" in dependencies:
                            dependencie_name=str(dependencies).split("==")[0]
                            dependencie_version= str(dependencies).split("==")[1]
                            python_dependencies.update({dependencie_name : dependencie_version})
                        else:
                            python_dependencies.update({str(dependencies) : "latest"})
                        
                if "pyproject.toml" in str(python_dep_file):
                    pyproject_file = [file
                    for file in archive.getmembers()
                    if "pyproject.toml" in file.name]
                    with io.TextIOWrapper(
                    archive.extractfile(pyproject_file[0]),
                    encoding="utf-8",
                    ) as fd:
                        raw_config = fd.read()
                        parsed_toml = toml.loads(raw_config)
                        python_dependencies.update(parsed_toml["tool"]["poetry"]["dependencies"])

                if "requirements.txt" in str(python_dep_file):
                    requirements_file = [file
                    for file in archive.getmembers()
                    if "requirements.txt" in file.name]
                    with io.TextIOWrapper(
                    archive.extractfile(requirements_file[0]),
                    encoding="utf-8",
                    ) as fd:
                        for line in fd:
                            line = (str(line).replace("\n", "")).replace(" ", "")
                            if "<" in line:
                                dependencie_name=(line).split("<")[0]
                                dependencie_version= "<" + str(line).split("<")[1]
                                python_dependencies.update({dependencie_name : dependencie_version})
                            elif ">" in line:
                                dependencie_name=str(line).split(">")[0]
                                dependencie_version= ">" + str(line).split(">")[1]
                                python_dependencies.update({dependencie_name : dependencie_version})                           
                            elif "==" in line:
                                dependencie_name=str(line).split("==")[0]
                                dependencie_version= str(line).split("==")[1]
                                python_dependencies.update({dependencie_name : dependencie_version})
                            else:
                                python_dependencies.update({str(line) : "latest"})

        return python_dependencies
                    

async def main():
    python = PythonHandler()
    artefact_name = "ska-ser-skallop"
    # artefact_name = "ska-cicd-services-api"
    # artefact_name = "ska-tango-examples"
    # artefact_name = "ska-tmc-dishleafnode-mid"
    # result = await python.search_components("pypi-internal", artefact_name)
    version = "2.8.0"
    # version = "0.24.3"
    # version = "0.4.16"
    # version = "0.8.5"
    # for index in result:
    #     if result[index]["version"] == version :
    #         assets = await python.download(result[index]["id"])
            # print(assets)
    assets=['ska-ser-skallop-2.8.0.tar.gz','ska_ser_skallop-2.8.0-py3-none-any.whl']
    # assets=['ska-cicd-services-api-0.24.3.tar.gz', 'ska_cicd_services_api-0.24.3-py3-none-any.whl']
    # assets=['ska-tango-examples-0.4.16.tar.gz', 'ska_tango_examples-0.4.16-py3-none-any.whl']
    # assets=['ska-tmc-dishleafnode-mid-0.8.5.tar.gz', 'ska_tmc_dishleafnode_mid-0.8.5-py3-none-any.whl']
    for asset in assets:
        if re.search(r".tar.gz", asset):
            print(asset)
            python_dependencies = python.get_python_dependencies(asset)

            print("Python Dependencies: ")
            for dependecy_name in python_dependencies:
                print("Name: ",dependecy_name, "  Version:", python_dependencies[dependecy_name])

            os.remove(asset)
            

if __name__ == "__main__":
   asyncio.run(main())